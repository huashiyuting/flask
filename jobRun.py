'''
@Author: hua
@Date: 2019-12-18 17:22:18
@description: 
@LastEditors  : hua
@LastEditTime : 2019-12-18 17:23:51
'''
import environment
environment.init("job") 
from app import app, sched
from flask_cors import CORS
# https://www.cnblogs.com/franknihao/p/7202253.html uwsgi配置
app = app
CORS(app, supports_credentials=True)
if __name__ == '__main__':
    #开始任务
    sched.start()
    app.debug = False
    app.run(host='0.0.0.0', port=500)